package be.tbb.newBank;

import be.tbb.newBank.data.BankTransactionDatabase;
import be.tbb.newBank.data.KlantDatabase;
import be.tbb.newBank.service.BankTransactionService;
import be.tbb.newBank.service.UserService;

import java.util.Scanner;

public class BankApp {
    public static void main(String[] args) {
        KlantDatabase klantDatabase = new KlantDatabase();
        BankTransactionDatabase bankTransactionDatabase = new BankTransactionDatabase();
        UserService userService = new UserService();
        BankTransactionService bankTransactionService = new BankTransactionService();

        klantDatabase.generateData();
        bankTransactionDatabase.generateData();

        boolean wantContinue = true;

        while(wantContinue) {
            if (userService.isLoggedIIn()) {
                userService.showMenu(userService.getLoggedInUser(), userService, bankTransactionService);
            } else {
                userService.loginProcess();
                if (userService.isLoggedIIn()) {
                    userService.showMenu(userService.getLoggedInUser(), userService, bankTransactionService);
                }
            }

            System.out.println("X -> for Exit");
            System.out.println("Any Key -> for Continue");
            Scanner scanner = new Scanner(System.in);
            if (scanner.nextLine().equalsIgnoreCase("x")){
                wantContinue=false;
            }
        }

    }


}




