package be.tbb.newBank.service;

import be.tbb.newBank.data.BankTransactionDatabase;
import be.tbb.newBank.data.KlantDatabase;
import be.tbb.newBank.entity.BankTransaction;
import be.tbb.newBank.entity.User;

import java.time.Instant;
import java.util.Scanner;

public class BankTransactionService {

    public void moneyTransferProcess(User loggedInUser, UserService userService, BankTransactionService bankTransactionService) {

                String receiverIban = askIban();
                double bedrag = askBedrag(loggedInUser);
                for (User klant : KlantDatabase.getKlanten()) {
                    if (receiverIban.equalsIgnoreCase(klant.getIban())) {
                        klant.setSaldo(klant.getSaldo()+bedrag);
                        break;
                    }
                }

                loggedInUser.setSaldo(loggedInUser.getSaldo()-bedrag);

                BankTransactionDatabase.addBankTransaction(new BankTransaction(bedrag, loggedInUser.getIban(), receiverIban, Instant.now()));
                System.out.println("\nYou just sent "+bedrag+"$ to "+receiverIban);

                userService.showMenu(loggedInUser, userService, bankTransactionService);
    }

    public void printOutgoingTransactions(User loggedInUser, UserService userService, BankTransactionService bankTransactionService) {
        System.out.println("\n *** Outgoing Transactions *** \n");
        for (BankTransaction outgoingTransaction : BankTransactionDatabase.getBankTransactions()) {
            if (outgoingTransaction.getSenderIban().equalsIgnoreCase(loggedInUser.getIban())){
                System.out.println(outgoingTransaction);
            }
        }
        userService.showMenu(loggedInUser, userService, bankTransactionService);
    }

    public void printIncomingTransactions(User loggedInUser, UserService userService, BankTransactionService bankTransactionService) {
        System.out.println("\n *** Incoming Transactions *** \n ");
        for (BankTransaction incomingTransaction : BankTransactionDatabase.getBankTransactions()) {
            if (incomingTransaction.getReceiverIban().equalsIgnoreCase(loggedInUser.getIban())){
                System.out.println(incomingTransaction);
            }
        }
        userService.showMenu(loggedInUser, userService, bankTransactionService);
    }

    public String askIban() {

        boolean isIbanNotCorrect = true;
        String receiverIban = "";

        while (isIbanNotCorrect){
            System.out.println("Voer het IBAN in waarnaar u het bedrag wilt overschrijven : ");
            Scanner key = new Scanner(System.in);
            receiverIban = key.nextLine();
            for (User receiver : KlantDatabase.getKlanten()) {
                if (receiverIban.equalsIgnoreCase(receiver.getIban())) {
                    isIbanNotCorrect = false;
                    break;
                }
            }
            if (isIbanNotCorrect) {
                System.out.println("De IBAN die je heeft ingevoerd is niet correct, probeer nog een keer ");
            }
        }

        return receiverIban;
    }

    public double askBedrag(User loggedInUser) {
        boolean isInvalidInput = true;
        double bedrag = 0;

        while (isInvalidInput) {
            System.out.println("Voer het bedrag in: ");
            Scanner key = new Scanner(System.in);
            bedrag = key.nextDouble();

            if(bedrag <= 0 || bedrag > loggedInUser.getSaldo()){
                System.out.println("Ongeldig input, het bedrag moet meer dan 0 en minder dan je saldo zijn! Probeer maar nog een keer!");
            }else {
                isInvalidInput = false;
            }
        }

        return bedrag;
    }
}
