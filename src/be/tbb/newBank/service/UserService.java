package be.tbb.newBank.service;

import be.tbb.newBank.data.KlantDatabase;
import be.tbb.newBank.entity.User;

import java.util.Scanner;

public class UserService {
    private User loggedInUser = null;
    private boolean isLoggedIIn = false;

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }

    public boolean isLoggedIIn() {
        return isLoggedIIn;
    }

    public void setLoggedIIn(boolean loggedIIn) {
        this.isLoggedIIn = loggedIIn;
    }

    public void loginProcess(){

        // this method still needs refactoring. there are too much loops inside another loop

        Scanner keyboard = new Scanner(System.in);
        boolean ibanOrPincodeNotCorrect = true;
        while(ibanOrPincodeNotCorrect) {
            System.out.println("Voer jouw IBAN in: ");
            String iban = keyboard.nextLine();
            for (User klant : KlantDatabase.getKlanten()) {
                if (iban.equalsIgnoreCase(klant.getIban())) {
                    int i = 0;
                    while(i < 4) {
                        System.out.println("Voer jouw PinCode in: ");
                        int pinCode = keyboard.nextInt();
                        if (pinCode == (klant.getPinCode())) {
                            setLoggedIIn(true);
                            setLoggedInUser(klant);
                            break;
                        } else if (i == 3) {
                            System.out.println("3 verkeerde invoerpogingen, helaas wordt het programma afgesloten");
                            System.exit(0);
                        } else System.out.println("Opnieuw proberern,je heeft totaal maximaal aantal pogingen: 3");
                        i++;
                    }
                    ibanOrPincodeNotCorrect = false;
                }
            }
            if (ibanOrPincodeNotCorrect) {
                System.out.println("De IBAN die je heeft ingevoerd is niet correct, probeer nog een keer :");
            }
        }
    }


    public void showMenu(User loggedInUser, UserService userService, BankTransactionService bankTransactionService){
        System.out.format("\nNaam: %s%nFamilienaam: %s%nSaldo: %s%nRekeningsoort: %sREKENING%n",
                loggedInUser.getNaam(), loggedInUser.getFamilienaam(), loggedInUser.getSaldo(), loggedInUser.getSoortBankrekening());
        System.out.println("\nKies een van de onderstaande opties\n1-Incoming transactions\n2-Outgoing transactions\n3-Make transfer\n4-Sign out\n");

        switch (new Scanner(System.in).nextInt()) {
            case 1 -> bankTransactionService.printIncomingTransactions(loggedInUser, userService, bankTransactionService);
            case 2 -> bankTransactionService.printOutgoingTransactions(loggedInUser, userService, bankTransactionService);
            case 3 -> bankTransactionService.moneyTransferProcess(loggedInUser, userService, bankTransactionService);
            case 4 -> signOut();
        }
    }

    public void signOut() {
        setLoggedInUser(null);
        setLoggedIIn(false);
        System.out.println("Thank you for using Big Bank! Have a good day! Good bye!");
    }


}


