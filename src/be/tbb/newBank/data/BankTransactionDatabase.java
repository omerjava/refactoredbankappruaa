package be.tbb.newBank.data;

import be.tbb.newBank.entity.BankTransaction;
import java.time.Instant;
import java.util.Arrays;

public class BankTransactionDatabase {

    private static BankTransaction[] bankTransactions = {};

    public static BankTransaction[] getBankTransactions() {
        return bankTransactions;
    }

    public void setBankTransactions(BankTransaction[] bankTransactions) {
        BankTransactionDatabase.bankTransactions = bankTransactions;
    }

    public static void addBankTransaction(BankTransaction bankTransaction){
        bankTransactions = Arrays.copyOf(bankTransactions, bankTransactions.length+1);
        bankTransactions[bankTransactions.length-1] = bankTransaction;
    }

    public void generateData(){

        addBankTransaction(new BankTransaction("Loan Payment", "BE12421288351123", "BE12437211451265", 500, "Debt payment", Instant.now()));
        addBankTransaction(new BankTransaction("Credit Payment", "BE12437211451265", "BE12421288351123", 1500, "payment", Instant.now()));
        addBankTransaction(new BankTransaction("Mortgage Payment", "BE14546309805546", "BE12437211451265", 1500, "Credit ", Instant.now()));
        addBankTransaction(new BankTransaction("Credit Payment", "BE14546309805546", "BE12092373621837", 500, "Credit", Instant.now()));
        addBankTransaction(new BankTransaction("Credit Payment", "BE12437211451265", "BE12092373621837", 1500, "Credit payment", Instant.now()));
        addBankTransaction(new BankTransaction("Factuur", "BE14546309805546", "BE12421288351123", 150, "internet factuur", Instant.now()));
        addBankTransaction(new BankTransaction("Credit Card", "BE14546309805546", "BE12437211451265", 1000, "Credit payment", Instant.now()));
        addBankTransaction(new BankTransaction("Credit", "BE14546309805546", "BE12421288351123", 500, "Credit payment", Instant.now()));
        addBankTransaction(new BankTransaction("Loan", "BE14546309805546", "BE12421288351123", 1500, "Loan payment", Instant.now()));
        addBankTransaction(new BankTransaction("Credit Payment", "BE12421288351123", "BE12437211451265", 150, "Credit payment", Instant.now()));

    }

}
