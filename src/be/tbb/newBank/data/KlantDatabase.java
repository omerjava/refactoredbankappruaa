package be.tbb.newBank.data;

import be.tbb.newBank.entity.User;
import be.tbb.newBank.entity.SoortBankrekening;

import java.util.Arrays;

public class KlantDatabase {

    private static User[] klanten = {};

    public static User[] getKlanten() {
        return klanten;
    }

    public void setKlanten(User[] klanten) {
        KlantDatabase.klanten = klanten;
    }

    public void addKlant(User klant){
        klanten = Arrays.copyOf(klanten, klanten.length+1);
        klanten[klanten.length-1] = klant;
    }

    public void generateData(){

        addKlant(new User("Sharlotte", "Willems", "BE12437211451265", 5431, 8725, SoortBankrekening.SPAAR));
        addKlant(new User("Bart", "Merckx", "BE12421288351123", 1990, 59234, SoortBankrekening.BETAAL));
        addKlant(new User("Jos", "De Smet", "BE14546309805546", 1345, 5620, SoortBankrekening.BETAAL));
        addKlant(new User("Nick", "Maes", "BE64662105321213", 1123, 65360, SoortBankrekening.BETAAL));
        addKlant(new User("John", "De Vos", "BE12092373621837", 5505, 9823530, SoortBankrekening.SPAAR));

        System.out.println(Arrays.toString(getKlanten()));

    }
}
