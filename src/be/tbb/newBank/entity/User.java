package be.tbb.newBank.entity;

public class User {
    private String naam;
    private  String familienaam;
    private  String iban;
    private int pinCode;
    private double saldo;
    private SoortBankrekening soortBankrekening;

    public User(String naam, String familienaam, String iban, int pinCode, double saldo, SoortBankrekening soortBankrekening){
        setNaam(naam);
        setFamilienaam(familienaam);
        setIban(iban);
        setPinCode(pinCode);
        setSaldo(saldo);
        setSoortBankrekening(soortBankrekening);

    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getFamilienaam() {
        return familienaam;
    }

    public void setFamilienaam(String familienaam) {
        this.familienaam = familienaam;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public SoortBankrekening getSoortBankrekening() {
        return soortBankrekening;
    }

    public void setSoortBankrekening(SoortBankrekening soortBankrekening) {
        this.soortBankrekening = soortBankrekening;
    }

    @Override
    public String toString() {
        return "Klant{" +
                "naam='" + naam + '\'' +
                ", familienaam='" + familienaam + '\'' +
                ", iban='" + iban + '\'' +
                ", pinCode=" + pinCode +
                ", saldo=" + saldo +
                ", soortBankrekening=" + soortBankrekening +
                '}';
    }
}
