package be.tbb.newBank.entity;

import java.time.Instant;

public class BankTransaction {

    private String transactionName;
    private String senderIban;
    private String receiverIban;
    private double bedrag;
    private String comment;
    private Instant date;

    public BankTransaction() { }
    public BankTransaction(String transactionName, String senderIban, String receiverIban, double bedrag, String comment, Instant date) {
        this.transactionName = transactionName;
        this.senderIban = senderIban;
        this.receiverIban = receiverIban;
        this.bedrag = bedrag;
        this.comment = comment;
        this.date = date;
    }

    public BankTransaction(double bedrag, String senderIban, String receiverIban, Instant date) {

        this.senderIban = senderIban;
        this.receiverIban = receiverIban;
        this.bedrag = bedrag;
        this.date = date;
    }

    public String getTransactionName() {
        return transactionName;
    }

    public void setTransactionName(String transactionName) {
        this.transactionName = transactionName;
    }

    public String getSenderIban() {
        return senderIban;
    }

    public void setSenderIban(String senderIban) {
        this.senderIban = senderIban;
    }

    public String getReceiverIban() {
        return receiverIban;
    }

    public void setReceiverIban(String receiverIban) {
        this.receiverIban = receiverIban;
    }

    public double getBedrag() {
        return bedrag;
    }

    public void setBedrag(double bedrag) {
        this.bedrag = bedrag;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "BankTransaction{" +
                "transactionName='" + transactionName + '\'' +
                ", senderIban='" + senderIban + '\'' +
                ", receiverIban='" + receiverIban + '\'' +
                ", bedrag=" + bedrag +
                ", comment='" + comment + '\'' +
                ", date=" + date +
                '}';
    }
}

